import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:test_app_cc/applicant/dialog_form_applicant.dart';
import 'package:test_app_cc/assets.dart';
import 'package:test_app_cc/colors.dart';
import 'package:test_app_cc/db/data_base.dart';
import 'package:test_app_cc/db/modules/applicant.dart';

class ValidateApplicant extends StatefulWidget {

  final Applicant applicant;
  final Function() refreshList;

  ValidateApplicant({this.applicant, this.refreshList});

  @override
  _ValidateApplicantState createState() => _ValidateApplicantState();
}

class _ValidateApplicantState extends State<ValidateApplicant> {
  TextEditingController _observations  = TextEditingController();
  String _radioButtonValue = "Autorizado";
  List<String> _status = ["Autorizado", "Rechazado"];
  bool buttonEnabled = true;
  bool loading = false;
  String status, observations;

  @override
  void initState() {
    super.initState();
    setState(() {
      status = widget.applicant.status;
      observations = widget.applicant.observations;
    });
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);

    final mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.principalColor,
        centerTitle: true,
        title: Text("Perfil prospecto", style: TextStyle(
          color: AppColors.backgroundColor
        ),),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Container(
                  width: mediaQuery.width * 0.94,
                  child: Card(
                    color: AppColors.backgroundColor,
                    elevation: 10,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                      child: Column(
                        children: [
                          Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: widget.applicant.name+" "+widget.applicant.firstLastName+" ",
                                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold,
                                  color: AppColors.secondaryColor)
                                ),
                                widget.applicant.secondLastName != null ?
                                TextSpan(
                                    text: widget.applicant.secondLastName,
                                    style: TextStyle(fontSize: 25,
                                    fontWeight: FontWeight.bold, color: AppColors.secondaryColor)) : SizedBox()
                              ]
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(height: 16,),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(widget.applicant.street+ " " + widget.applicant.number,
                            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                            ),
                          ),
                          SizedBox(height: 7,),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(widget.applicant.neighboorhood+ " " + widget.applicant.zipCode,
                                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500))),
                          SizedBox(height: 7,),
                          Row(
                            children: [
                              Text("RFC: ", style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
                              Text(widget.applicant.rfc, style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400)),
                            ],
                          ),
                          SizedBox(height: 16,),
                          Text("Estado del aplicante", style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 25, color: AppColors.secondaryColor
                          ),),
                          Text(status, style: TextStyle(
                              fontSize: 20, color: status == "Enviado" ?
                          Colors.blue : status == "Autorizado" ?
                          Colors.green : Colors.red
                          )),
                          SizedBox(height: status =="Rechazado" ? 12 : 0,),
                          status == "Rechazado" ? Container(
                            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                            decoration: BoxDecoration(
                              border: Border.all(color: AppColors.secondaryColor),
                            ),
                            child: Text("Razon: "+observations, style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16, color: AppColors.secondaryColor
                            ),),
                          ) : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Text("Selecciona una opcion", style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.w500,
                  color: AppColors.secondaryColor
              ),),
              SizedBox(height: 10,),
              Container(
                width: mediaQuery.width * .7,
                child: RadioGroup.builder(
                    direction: Axis.horizontal,
                    activeColor: AppColors.radioButton,
                    groupValue: _radioButtonValue,
                    onChanged: (value) => setState(() {
                      _radioButtonValue = value;
                      if(_radioButtonValue == "Autorizado"){
                          buttonEnabled = true;
                      }else{
                        buttonEnabled = false;
                      }
                    }),
                    items: _status,
                    itemBuilder: (item) => RadioButtonBuilder(
                      item,
                    ),),
              ),
              SizedBox(height: _radioButtonValue == "Rechazado" ? 20 : 0,),
              _radioButtonValue == "Rechazado" ? Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                    border: Border.all(color: AppColors.secondaryColor)
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text("Razón del rechazo", style: TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor
                      ),),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                      child: FormBuilderTextField(
                        controller: _observations,
                        maxLength: 200,
                        minLines: 1,
                        maxLines: 6,
                        name: "Observaciones",
                        keyboardType: TextInputType.multiline,
                        textCapitalization: TextCapitalization.sentences,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                              color: Colors.black
                          ),
                          icon: Icon(Icons.text_fields, color: AppColors.radioButton,),
                          focusColor: AppColors.radioButton,
                        ),
                        validator: FormBuilderValidators.required(context, errorText: "Este campo no puede ser vacio"),
                        textInputAction: TextInputAction.done,
                        onChanged: (value){
                          if(_observations.text.isNotEmpty){
                            setState(() {
                              buttonEnabled = true;
                            });
                          }if(_observations.text.isEmpty){
                            setState(() {
                              buttonEnabled = false;
                            });
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ) : SizedBox(),

              SizedBox(height: 25,),
              loading ? CircularProgressIndicator() : SizedBox(
                height: 50,
                width: mediaQuery.width * .93,
                child: TextButton(
                  onPressed: () => buttonEnabled ? _update() : () {}, child: Text("Guardar", style: TextStyle(
                    color: AppColors.backgroundColor, fontSize: 18
                ),),
                  style: ButtonStyle(
                      backgroundColor: buttonEnabled ? MaterialStateProperty.all(AppColors.secondaryColor) :
                      MaterialStateProperty.all(AppColors.secondaryColor.withOpacity(.5))
                  ),),
              ),
              SizedBox(height: 25,),
            ],
          ),
        ),
      ),
    );
  }

  _update() async{
    setState(() {
      loading = true;
    });
    try {
      await DBProvider.bd.updateStatus(id: widget.applicant.id, status: _radioButtonValue,
      observations: _observations.text);
      setState(() {
        status = _radioButtonValue;
        observations = _observations.text;
        _observations.clear();
      });
      _dialogSuccesful();
      widget.refreshList();
    }catch(_){}
    setState(() {
      loading = false;
    });
  }

  Future<void> _dialogSuccesful() async {
    await showPlatformDialog(
        context: context, builder: (_) => DialogFormApplicant(
      title: "Aviso",
      contextDialog: "Se actualizo el estado del prospecto correctamente",
      picture: Assets.successful,));
  }
}
