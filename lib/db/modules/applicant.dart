class Applicant{


  String id;
  String name;
  String firstLastName;
  String secondLastName;
  String street;
  String number;
  String neighboorhood;
  String zipCode;
  String telephone;
  String status;
  String observations;
  String rfc;

  Applicant({
      this.id,
      this.name,
      this.firstLastName,
      this.secondLastName,
      this.street,
      this.number,
      this.neighboorhood,
      this.zipCode,
      this.telephone,
      this.status,
      this.observations,
      this.rfc});
}