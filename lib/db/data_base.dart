import 'dart:io';
import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';
import 'package:test_app_cc/db/modules/applicant.dart';
import 'package:test_app_cc/db/modules/documents_applicant.dart';

class DBProvider{
  DBProvider._();
  static final DBProvider bd = DBProvider._();
  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async{
    var databasesPath = await getDatabasesPath();
    String path  = join(databasesPath, "Appcc.db");
    return await openDatabase(path, version: 1, onOpen: (db){}, onCreate: _createTables);
  }

  void _createTables(Database db, int version) async {

    final String CREATE_APPLICANT_TABLE = "CREATE TABLE applicant ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "name TEXT NOT NULL,"
        "firstLastName TEXT NOT NULL,"
        "secondLastName TEXT NOT NULL,"
        "street TEXT NOT NULL,"
        "number TEXT NOT NULL,"
        "neighboorhood TEXT NOT NULL,"
        "zipCode TEXT NOT NULL,"
        "telephone TEXT NOT NULL,"
        "status TEXT NOT NULL,"
        "observations TEXT,"
        "rfc TEXT NOT NULL);";

    final String CREATE_DOCUMENTS_APPLICANT_TABLE = "CREATE TABLE documents_applicant ("
        "nameDocument TEXT NOT NULL,"
        "path TEXT NOT NULL,"
        "applicant_id TEXT NOT NULL);";

    List<String> createTables = [CREATE_APPLICANT_TABLE, CREATE_DOCUMENTS_APPLICANT_TABLE];

    for(int i = 0; i < createTables.length; i++){
      await db.execute(createTables[i]);
    }
  }

  newRegisterApplicant({String name, String firstLastName, String secondLastName, String street,
    String number, String neighboorhood, String zipcode, String telephone,
    String rfc, String status, List<String> listDocuments, List<String> paths}) async {

    final db = await database;

    try {
      final id = await db.rawInsert(
          "INSERT INTO applicant (name, firstLastName, secondLastName, street, "
              "number, neighboorhood, zipCode, telephone, status, rfc)"
              " VALUES (?,?,?,?,?,?,?,?,?,?)",
          [
            name,
            firstLastName,
            secondLastName,
            street,
            number,
            neighboorhood,
            zipcode,
            telephone,
            status,
            rfc
          ]);

      for (int i = 0; i < listDocuments.length; i++) {

          await db.rawInsert(
              "INSERT INTO documents_applicant (nameDocument, path, applicant_id) VALUES (?,?,?)",
              [
                listDocuments[i],
                paths[i],
                id.toString()
              ]);
      }

    } catch (_) {
    }
  }

  Future<List<Applicant>> getApplicantsFromDataBase() async {
    final db = await database;
    List<Applicant> list = [];
    var selectFromApplicant = await db.rawQuery('SELECT * FROM applicant');

    for (int i = 0; i < selectFromApplicant.length; i++) {
      final row = selectFromApplicant[i];
        list.add(Applicant(
          id: row['id'].toString(),
          name: row['name'],
          firstLastName: row['firstLastName'],
          secondLastName: row['secondLastName'],
          status: row['status'],
          rfc: row['rfc'],
          telephone: row['telephone'],
          street: row['street'],
          number: row['number'],
          neighboorhood: row['neighboorhood'],
          observations: row['observations'],
          zipCode: row['zipCode']
        ));
    }

    return list;
  }

  Future<List<String>> getPathsApplicant(String idApplicant) async {
    final db = await database;
    List<String> listPathsDocuments = [];

    var selectFromDocuments = await db.rawQuery('SELECT path FROM documents_applicant WHERE applicant_id = "$idApplicant"');

    for (int i = 0; i < selectFromDocuments.length; i++) {
      listPathsDocuments.add(selectFromDocuments[i]["path"]);
    }
    return listPathsDocuments;
  }

  Future<List<String>> getDocumentsApplicant(String idApplicant) async {
    final db = await database;
    List<String> listNameDocuments = [];

    var selectFromDocuments = await db.rawQuery('SELECT nameDocument FROM documents_applicant WHERE applicant_id = "$idApplicant"');

    for (int i = 0; i < selectFromDocuments.length; i++) {

      listNameDocuments.add(selectFromDocuments[i]["nameDocument"]);
    }
    return listNameDocuments;
  }

  updateStatus({String id, String status, String observations}) async{
    final db = await database;
    await db.rawQuery('UPDATE applicant SET status = "$status", observations = "$observations" WHERE id = "$id"');
  }
}