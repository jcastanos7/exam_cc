import 'package:flutter/material.dart';
import 'package:test_app_cc/authorization/validate_applicant.dart';
import 'package:test_app_cc/colors.dart';
import 'package:test_app_cc/db/modules/applicant.dart';
import 'package:test_app_cc/list_applicant/view_all_information_applicant.dart';

import '../assets.dart';

class ApplicantCard extends StatefulWidget {
  final Function() refreshList;
  final Applicant applicant;
  final bool isAuthorizationPage;

  ApplicantCard({this.applicant, this.isAuthorizationPage = false,
    this.refreshList});

  @override
  _ApplicantCardState createState() => _ApplicantCardState();
}

class _ApplicantCardState extends State<ApplicantCard> {

 @override
  Widget build(BuildContext context) {
   final mediaQuery = MediaQuery.of(context).size;

    return InkWell(
      onTap: (){
        widget.isAuthorizationPage ? Navigator.push(context,
            MaterialPageRoute(builder: (context)=> ValidateApplicant(applicant: widget.applicant,
            refreshList: widget.refreshList,)))
            : Navigator.push(context, MaterialPageRoute(builder: (context)=>
        ViewAllInformationApplicant(applicant: widget.applicant,)));
      },
      child: Card(
        color: AppColors.backgroundColor,
        child: Row(
          children: [
            Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(
                      Assets.profile,
                    ),
                  ),
                )),
            SizedBox(width: 8,),
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: mediaQuery.width * 0.6,
                    child: Text(widget.applicant.name +" "+widget.applicant.firstLastName
                      +" "+widget.applicant.secondLastName, style: TextStyle(fontSize: 20, color: AppColors.secondaryColor,
                    fontWeight: FontWeight.w500),
                    overflow: TextOverflow.ellipsis,),
                  ),
                  SizedBox(height: 8,),
                  Text(widget.applicant.status, style: TextStyle(fontSize: 16, color: widget.applicant.status == "Enviado" ?
                  Colors.blue : widget.applicant.status == "Autorizado" ?
                  Colors.green : Colors.red)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
