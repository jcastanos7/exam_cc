import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:test_app_cc/applicant/dialog_form_applicant.dart';
import 'package:test_app_cc/db/data_base.dart';
import 'package:test_app_cc/db/modules/applicant.dart';
import 'package:test_app_cc/list_applicant/applicant_card.dart';
import 'package:test_app_cc/list_applicant/empty_list_page.dart';

class ListApplicant extends StatefulWidget {
  final bool isAuthorizationPage;

  ListApplicant({this.isAuthorizationPage});

  @override
  _ListApplicantState createState() => _ListApplicantState();
}

class _ListApplicantState extends State<ListApplicant> {
  List<Applicant> applicantList = [];
  bool loading = true;

  @override
  void initState() {
    super.initState();
    _getApplicantList();
  }

  @override
  Widget build(BuildContext context) {
    if(loading){
      return Center(child: CircularProgressIndicator());
    }

    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            applicantList.isNotEmpty ? ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: applicantList.length,
                itemBuilder: (context,index){
              return ApplicantCard(applicant: applicantList[index],
                isAuthorizationPage: widget.isAuthorizationPage, refreshList: _refreshList,);
            }) : EmptyListPage(),
          ],
        ),
      ),
    );
  }

  _getApplicantList() async {
    setState(() {
      loading = true;
    });
    try{
      applicantList = await DBProvider.bd.getApplicantsFromDataBase();
    }catch(_){
      _dialogError();
    }
    setState(() {
      loading = false;
    });
  }

  _refreshList(){
    applicantList.clear();
    _getApplicantList();
  }
  Future<void> _dialogError() async {
    await showPlatformDialog(
        context: context, builder: (_) => DialogFormApplicant(
      title: "Error",
      contextDialog: "Error al obtener la información, intente mas tarde.",));
  }

}
