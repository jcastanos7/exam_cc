import 'package:flutter/material.dart';
import 'package:test_app_cc/assets.dart';

class EmptyListPage extends StatefulWidget {

  @override
  _EmptyListPageState createState() => _EmptyListPageState();
}

class _EmptyListPageState extends State<EmptyListPage> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    double widthScreen = mediaQuery.width;
    double top = AppBar().preferredSize.height;

    return Container(
      height: mediaQuery.height - top,
      width: widthScreen,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        Container(
          width: 100,
          height: 100,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(
              Assets.empty,
            ),
          ),
        )),
          SizedBox(height: 16,),
          Text("No se encontraron resultados", style: TextStyle(
            color: Colors.black, fontSize: 30,
          ), textAlign: TextAlign.center,)
        ],
      ),
    );
  }
}
