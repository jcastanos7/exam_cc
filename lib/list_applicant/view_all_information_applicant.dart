import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:test_app_cc/assets.dart';
import 'package:test_app_cc/colors.dart';
import 'package:test_app_cc/db/data_base.dart';
import 'package:test_app_cc/db/modules/applicant.dart';
import 'package:test_app_cc/list_applicant/pfd_page.dart';

class ViewAllInformationApplicant extends StatefulWidget {

  final Applicant applicant;

  ViewAllInformationApplicant({this.applicant});

  @override
  _ViewAllInformationApplicantState createState() => _ViewAllInformationApplicantState();
}

class _ViewAllInformationApplicantState extends State<ViewAllInformationApplicant> {
  List<String> listDocuments = [];
  List<String> listPath = [];
  bool loading = true;

  @override
  void initState() {
    super.initState();
    _getDocuments();
  }


  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);

    final mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.principalColor,
        title: Text("Perfil del Prospecto", style: TextStyle(
          color: AppColors.backgroundColor
        ),),
        centerTitle: true,
      ),
      body: loading ? Center(child: CircularProgressIndicator()) : Container(
        width: mediaQuery.width,
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                  width: mediaQuery.width,
                  height: 200,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(
                        Assets.background,
                      ),
                    ),
                  )),
              Center(
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        width: 200,
                        height: 200,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage(
                              Assets.man,
                            ),
                          ),
                        )),
                    SizedBox(
                      height: 100,
                      width: mediaQuery.width*0.9,
                      child: Card(
                        color: AppColors.backgroundColor,
                        elevation: 5,
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                          child: Column(
                            children: [
                              Text(widget.applicant.name +" "+ widget.applicant.firstLastName+
                              " "+ widget.applicant.secondLastName, style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w700, color: AppColors.secondaryColor
                              ),),
                              Text(widget.applicant.rfc, style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500
                              )),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Icon(Icons.location_on_rounded, size: 45, color: AppColors.secondaryColor,),
                        SizedBox(width: 10,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.applicant.street+" "+widget.applicant.number, style:
                              TextStyle(fontSize: 18),),
                            Text(widget.applicant.neighboorhood+ " "+"Cp."+ widget.applicant.zipCode, style:
                            TextStyle(fontSize: 18)),
                          ],
                        ),
                        ],
                      ),
                    ),
                    SizedBox(height: 12,),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(Icons.phone, size: 45, color: AppColors.secondaryColor,),
                          SizedBox(width: 10,),
                          Text(widget.applicant.telephone, style: TextStyle(
                              fontSize: 18
                          ),),
                        ],
                      ),
                    ),
                    SizedBox(height: 12,),
                    Text("Documentos", style: TextStyle(
                    fontSize: 30, color: AppColors.secondaryColor, fontWeight: FontWeight.w500
                    )),
                    SizedBox(height: 16,),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Selecciona el documento", style: TextStyle(
                            fontSize: 20, color: AppColors.principalColor, fontWeight: FontWeight.w500
                        )),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        width: mediaQuery.width*.8,
                        child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: listDocuments.length,
                            itemBuilder: (context, index){
                              final path = listPath[index];

                              return InkWell(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> PdfPage(
                                    name: listDocuments[index],
                                    path: path,
                                  )));
                                },
                                child: Container(
                                  margin: EdgeInsets.symmetric(horizontal: 20),
                                  child: Row(
                                    children: [
                                      Icon(Icons.description, color: AppColors.secondaryColor,),
                                      SizedBox(width: 6,),
                                      Container(
                                        width: mediaQuery.width *.6,
                                        child: Text(listDocuments[index], style: TextStyle(
                                            color: AppColors.secondaryColor, fontWeight: FontWeight.w500
                                        ), overflow: TextOverflow.ellipsis,),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }),
                      ),
                    ),
                    SizedBox(height: 30,),
                    Text("Estado del prospecto", style: TextStyle(
                      fontSize: 30, color: AppColors.secondaryColor, fontWeight: FontWeight.w500
                    ),),
                    SizedBox(height: 16,),
                    Text(widget.applicant.status, style: TextStyle(
                        fontSize: 25, color: widget.applicant.status == "Enviado" ?
                        Colors.blue : widget.applicant.status == "Autorizado" ?
                    Colors.green : Colors.red
                    )),
                    widget.applicant.status == "Rechazado" ? SizedBox(
                      height: 16,
                    ) :SizedBox(),
                    widget.applicant.status == "Rechazado" ? Container(
                      width: mediaQuery.width*.9,
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      decoration: BoxDecoration(
                          border: Border.all(color: AppColors.secondaryColor)
                      ),
                      child: Text("Razon: "+widget.applicant.observations, style: TextStyle(
                        color: AppColors.secondaryColor, fontSize: 20, fontWeight: FontWeight.w500
                      ),),
                    ) : SizedBox(),
                    SizedBox(height: 50,),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getDocuments() async{

    try{
      listDocuments = await DBProvider.bd.getDocumentsApplicant(widget.applicant.id);
      listPath = await DBProvider.bd.getPathsApplicant(widget.applicant.id);
    }catch(_){}
    setState(() {
      loading = false;
    });
  }
}
