import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:test_app_cc/colors.dart';

class PdfPage extends StatefulWidget {
  final String path;
  final String name;
  
  PdfPage({this.path, this.name});

  @override
  _PdfPageState createState() => _PdfPageState();
}

class _PdfPageState extends State<PdfPage> {
  PdfController pdfController ;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    _getPdf();
  }
  
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.principalColor,
        centerTitle: true,
        title: Text(widget.name, style: TextStyle(
          color: AppColors.backgroundColor
        ),),
      ),
      body: loading ? Center(child: CircularProgressIndicator()) : Container(
        child: PdfView(
          controller: pdfController,
          errorBuilder: (e){
            return Container(
              child: Text("Error al cargar el documento"),
            );
          },
        ),
      ),
    );
  }

  _getPdf() async{

    try{
    pdfController = PdfController(
      document: PdfDocument.openFile(widget.path),
    );
    }catch(e,s){
      print(s);
    }
    setState(() {
      loading = false;
    });
  }
}
