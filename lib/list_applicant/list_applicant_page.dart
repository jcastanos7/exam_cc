import 'package:flutter/material.dart';
import 'package:test_app_cc/colors.dart';
import 'package:test_app_cc/list_applicant/list_applicant.dart';

class ListApplicantPage extends StatefulWidget {
  final bool isAuthorizationPage;

  ListApplicantPage({this.isAuthorizationPage});

  @override
  _ListApplicantPageState createState() => _ListApplicantPageState();
}

class _ListApplicantPageState extends State<ListApplicantPage> {
  String listApplicant = "Lista de prospectos";
  String authorizationPage = "Autorizaciones";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.principalColor,
        title: Text(widget.isAuthorizationPage ? authorizationPage : listApplicant,
        style: TextStyle(color: AppColors.backgroundColor),),
        centerTitle: true,
      ),
      body: Container(
        child: ListApplicant(isAuthorizationPage: widget.isAuthorizationPage),
      ),
    );
  }
}
