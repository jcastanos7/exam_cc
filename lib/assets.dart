final root = 'assets/';

class Assets{
  static get empty => "${root}empty.png";
  static get profile => "${root}profile.png";
  static get man => "${root}man.png";
  static get background => "${root}background.jpeg";
  static get splash => "${root}bolsa-de-dinero.png";
  static get welcome => "${root}bienvenido.png";
  static get welcomeSecond => "${root}prestamo.svg";
  static get successful => "${root}succesful.svg";
  static get error => "${root}error-404.svg";
  static get warning => "${root}advertencia.svg";
}