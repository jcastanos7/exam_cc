import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_app_cc/applicant/form_applicant.dart';
import 'package:test_app_cc/assets.dart';
import 'package:test_app_cc/authorization/authorization_page.dart';
import 'package:test_app_cc/colors.dart';
import 'package:test_app_cc/list_applicant/list_applicant_page.dart';
import 'package:test_app_cc/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: SplashPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);

    final mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.principalColor,
            title: Text("Bienvenido", style: TextStyle(
                color: AppColors.backgroundColor
            ),),
            centerTitle: true,
          ),
          body: Container(
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(height: 16,),
                Container(
                  width: mediaQuery.width,
                  height: mediaQuery.height * .3,
                  child: Card(
                    color: AppColors.backgroundColor,
                    elevation: 5,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 100,
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>
                                    FormPage()));
                              },
                              child: Column(
                                children: [
                                  Container(
                                    width: 100,
                                    height: 100,
                                    decoration: BoxDecoration(
                                      color: AppColors.secondaryColor,
                                      shape: BoxShape.circle
                                    ),
                                    child: Icon(Icons.add, color: AppColors.backgroundColor, size: 40,),
                                  ),
                                  SizedBox(height: 6,),
                                  Text("Crear \nprospecto", style: TextStyle(
                                    color: AppColors.secondaryColor,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400
                                  ), textAlign: TextAlign.center)
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: 100,
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => ListApplicantPage(isAuthorizationPage: false,)
                                ));
                              },
                              child: Column(
                                children: [
                                  Container(
                                    width: 100,
                                    height: 100,
                                    decoration: BoxDecoration(
                                        color: AppColors.secondaryColor,
                                        shape: BoxShape.circle
                                    ),
                                      child: Icon(Icons.list, color: AppColors.backgroundColor, size: 40,),
                                  ),
                                  SizedBox(height: 6,),
                                  Text("Lista de \nprospectos", style: TextStyle(
                                      color: AppColors.secondaryColor,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400
                                  ), textAlign: TextAlign.center)
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: 100,
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>
                                    AuthorizationPage()));
                              },
                              child: Column(
                                children: [
                                  Container(
                                    width: 100,
                                    height: 100,
                                    decoration: BoxDecoration(
                                        color: AppColors.secondaryColor,
                                        shape: BoxShape.circle
                                    ),
                                    child: Icon(Icons.done, color: AppColors.backgroundColor, size: 40,),
                                  ),
                                  SizedBox(height: 6,),
                                  Text("Autorizaciones", style: TextStyle(
                                      color: AppColors.secondaryColor,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400
                                  ), textAlign: TextAlign.center,)
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16,),
                Container(
                  width: mediaQuery.width,
                  height: mediaQuery.height *.53,
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(Assets.welcomeSecond, height: 150, width: 150, color: AppColors.backgroundSecondary,),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
  }
}
