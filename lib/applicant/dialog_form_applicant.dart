import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_app_cc/colors.dart';

class DialogFormApplicant extends StatefulWidget {

  final String title;
  final String contextDialog;
  final bool isExit;
  final String picture;

  DialogFormApplicant({this.title, this.contextDialog, this.isExit = false, this.picture});

  @override
  _DialogFormApplicantState createState() => _DialogFormApplicantState();
}

class _DialogFormApplicantState extends State<DialogFormApplicant> {
  @override
  Widget build(BuildContext context) {
    return PlatformAlertDialog(
      title: Center(child: Text(widget.title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500,
      color: AppColors.secondaryColor),)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(widget.picture, width: 30, height: 30,),
          SizedBox(height: 6,),
          Text(widget.contextDialog,
            style: TextStyle(color: AppColors.secondaryColor,fontSize: 16,
              fontWeight: FontWeight.w400,), textAlign: TextAlign.center,
          maxLines: 2,),
        ],
      ),
      actions: <Widget>[
        SizedBox(width: 15,),
        TextButton(
          onPressed: !widget.isExit ? () => Navigator.of(context).pop() : (){
            Navigator.pop(context);
            Navigator.pop(context);
          },
          child: Text("Cerrar", style: TextStyle(color: AppColors.secondaryColor,
              fontSize: 14, fontWeight: FontWeight.w500),),
        )
      ],
    );
  }
}
