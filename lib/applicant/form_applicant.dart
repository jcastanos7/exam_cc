import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:test_app_cc/applicant/dialog_form_applicant.dart';
import 'package:test_app_cc/assets.dart';
import 'package:test_app_cc/colors.dart';
import 'package:test_app_cc/db/data_base.dart';
import 'package:test_app_cc/patterns.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';

class FormPage extends StatefulWidget {

  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  List<String> _listNamesDocuments = [];
  List<String> _listPaths = [];
  bool buttonEnabled = false;
  bool _loading = false;
  final _formKey = GlobalKey<FormBuilderState>();
  TextEditingController _name  = TextEditingController();
  TextEditingController _firstLastName  = TextEditingController();
  TextEditingController _secondLastName  = TextEditingController();
  TextEditingController _street  = TextEditingController();
  TextEditingController _numberStreet  = TextEditingController();
  TextEditingController _neighborhood  = TextEditingController();
  TextEditingController _cp  = TextEditingController();
  TextEditingController _cellPhone  = TextEditingController();
  TextEditingController _rfc  = TextEditingController();

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    final mediaQuery = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.principalColor,
          title: Text("Registro", style: TextStyle(
            color: AppColors.backgroundColor
          ),),
          centerTitle: true,
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 16,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                  child: FormBuilder(
                    key: _formKey,
                    child: Column(
                      children: [
                        FormBuilderTextField(
                          controller: _name,
                          name: "Nombre",
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: AppColors.secondaryColor, width: 2.0),
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            labelStyle: TextStyle(
                              color: AppColors.secondaryColor
                            ),
                            labelText: "Nombre*",
                            icon: Icon(Icons.person, color: AppColors.secondaryColor,),
                            focusColor: AppColors.secondaryColor,
                            hintText: "Nombre*",
                          ),
                          validator: FormBuilderValidators.match(context, regexOnlyLetters,
                              errorText: "No se pueden introducir números"),
                          textInputAction: TextInputAction.next,
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          controller: _firstLastName,
                          name: "Primer apellido",
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                              focusColor: AppColors.secondaryColor,
                                labelText: "Primer apellido*",
                                icon: Icon(Icons.person, color: AppColors.secondaryColor,),
                                hintText: "Primer apellido*",
                              labelStyle: TextStyle(
                                  color: AppColors.secondaryColor
                              ),
                            ),
                          textInputAction: TextInputAction.next,
                          validator: FormBuilderValidators.match(context, regexOnlyLetters,
                                errorText: "No se pueden introducir números"),
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          controller: _secondLastName,
                          name: "Segundo apellido",
                          keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                labelStyle: TextStyle(
                                    color: AppColors.secondaryColor
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                focusColor: AppColors.secondaryColor,
                                labelText: "Segundo apellido",
                                icon: Icon(Icons.person, color: AppColors.secondaryColor,),
                                hintText: "Segundo apellido"
                            ),
                          validator: FormBuilderValidators.match(context, regexOnlyLetters,
                              errorText: "No se pueden introducir números"),
                          textInputAction: TextInputAction.next,
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          controller: _street,
                          name: "Calle",
                          keyboardType: TextInputType.streetAddress,
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                  color: AppColors.secondaryColor
                              ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                icon: Icon(Icons.place, color: AppColors.secondaryColor,),
                                labelText: "Calle*",
                                hintText: "Calle*",
                                focusColor: AppColors.secondaryColor,
                            ),
                          textInputAction: TextInputAction.next,
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          controller: _numberStreet,
                          name: "Número",
                          keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                  color: AppColors.secondaryColor
                              ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                icon: Icon(Icons.add_location_sharp, color: AppColors.secondaryColor,),
                                hintText: "Número*",
                                labelText: "Número*",
                                focusColor: AppColors.secondaryColor,
                            ),
                          textInputAction: TextInputAction.next,
                          validator: FormBuilderValidators.match(context, regexOnlyNumbers,
                              errorText: "No se admiten caracteres especiales"),
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          controller: _neighborhood,
                          name: "Colonia",
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.words,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                  color: AppColors.secondaryColor
                              ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                icon: Icon(Icons.place_sharp, color: AppColors.secondaryColor,),
                                hintText: "Colonia*",
                                labelText: "Colonia*",
                                focusColor: AppColors.secondaryColor,
                            ),
                          textInputAction: TextInputAction.next,
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          controller: _cp,
                          maxLength: 5,
                          name: "Código Postal",
                          keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                  color: AppColors.secondaryColor
                              ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                icon: Icon(Icons.add_location_alt, color: AppColors.secondaryColor,),
                                labelText: "Código Postal*",
                                hintText: "Código Postal*",
                                focusColor: AppColors.secondaryColor,
                            ),
                          validator: FormBuilderValidators.match(context, regexOnlyNumbers,
                              errorText: "No se admiten caracteres especiales"),
                          textInputAction: TextInputAction.next,
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          controller: _cellPhone,
                          name: "Teléfono",
                          maxLength: 10,
                          keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                  color: AppColors.secondaryColor
                              ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                icon: Icon(Icons.phone_android, color: AppColors.secondaryColor,),
                                hintText: "Teléfono*",
                                labelText: "Teléfono*",
                                focusColor: AppColors.secondaryColor,
                            ),
                          textInputAction: TextInputAction.next,
                          validator: FormBuilderValidators.match(context, regexOnlyNumbers,
                              errorText: "No se admiten caracteres especiales"),
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        FormBuilderTextField(
                          maxLength: 13,
                          controller: _rfc,
                          textCapitalization: TextCapitalization.characters,
                          name: "RFC",
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                  color: AppColors.secondaryColor
                              ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(color: AppColors.secondaryColor, width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                icon: Icon(Icons.perm_contact_cal, color: AppColors.secondaryColor,),
                                hintText: "RFC*",
                                labelText: "RFC*",
                                focusColor: AppColors.secondaryColor,
                            ),
                          textInputAction: TextInputAction.done,
                          validator: FormBuilderValidators.match(context, regexRfc,
                              errorText: "RFC invalido"),
                          onSubmitted: (value){
                            _validateButton();
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Text("Carga los documentos necesarios", style: TextStyle(
                  color: AppColors.secondaryColor, fontSize: 18, fontWeight: FontWeight.w500
                ),),
                Text("(Solo admite PDF)", style: TextStyle(
                    color: AppColors.secondaryColor, fontSize: 12, fontWeight: FontWeight.w500
                ),),
                SizedBox(
                  height: 10,
                ),
                IconButton(
                    onPressed: () async{
                  FilePickerResult result = await FilePicker.platform.pickFiles(
                      type: FileType.custom,
                      allowedExtensions: ['pdf'],
                      allowMultiple: true);
                  if(result != null) {
                    setState(() {
                      _listNamesDocuments = result.names;
                      _listPaths = result.paths;
                    });
                    _validateButton();
                  }
                }, icon: Icon(Icons.add)),
                SizedBox(height: 10),
                _listNamesDocuments.isNotEmpty && _listPaths.isNotEmpty ?
                    Tags(
                      itemCount:_listNamesDocuments.length,
                      itemBuilder: (index){
                        final item = _listNamesDocuments[index];

                        if(_listNamesDocuments[index].contains(".pdf")) {

                          return Container(
                            width: mediaQuery.width * .9,
                            child: ItemTags(
                                activeColor: AppColors.backgroundColor,
                                textActiveColor: AppColors.secondaryColor,
                                index: index,
                                title: item,
                                textStyle: TextStyle(fontSize: 16),
                                textOverflow: TextOverflow.ellipsis,
                                removeButton: ItemTagsRemoveButton(
                                  onRemoved: () {
                                    setState(() {
                                      _listNamesDocuments.removeAt(index);
                                      _listPaths.removeAt(index);
                                    });
                                    _validateButton();
                                    return true;
                                  },
                                )),
                          );
                        }else{

                          _listNamesDocuments.removeAt(index);
                          _listPaths.removeAt(index);

                          return SizedBox();
                        }
                      },
                    ): SizedBox(),

                SizedBox(
                  height: 20,
                ),
                _loading ? CircularProgressIndicator() : SizedBox(
                  height: 50,
                  width: mediaQuery.width * .8,
                  child: TextButton(
                    onPressed: () => buttonEnabled ? _onSubmit() : () {}, child: Text("Crear", style: TextStyle(
                    color: Colors.white
                  ),),
                  style: ButtonStyle(
                     backgroundColor: buttonEnabled ? MaterialStateProperty.all(AppColors.secondaryColor) :
                     MaterialStateProperty.all(AppColors.secondaryColor.withOpacity(.5))
                  ),),
                ),
                SizedBox(
                  height: 25,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _onSubmit() async{
      final form = _formKey.currentState;

      if(form.saveAndValidate()){
        setState(() {
          _loading = true;
        });

        await DBProvider.bd.newRegisterApplicant(
          name: _name.text,
          firstLastName: _firstLastName.text,
          secondLastName: _secondLastName.text,
          neighboorhood: _neighborhood.text,
          number: _numberStreet.text,
          street: _street.text,
          telephone: _cellPhone.text,
          zipcode: _cp.text,
          status: "Enviado",
          listDocuments: _listNamesDocuments,
          paths: _listPaths,
          rfc: _rfc.text
        );
        setState(() {
          _loading = false;
        });

        _dialogSuccess();
        setState(() {
          _listNamesDocuments.clear();
          _listPaths.clear();
          _name.clear();
          _firstLastName.clear();
          _secondLastName.clear();
          _neighborhood.clear();
          _numberStreet.clear();
          _street.clear();
          _cellPhone.clear();
          _cp.clear();
          _rfc.clear();
        });
      }else{
        setState(() {
          _loading = false;
        });
        _dialogWrongValue();
      }
  }

  Future<void> _dialogSuccess() async {
    await showPlatformDialog(
        context: context, builder: (_) => DialogFormApplicant(
      title: "Registrado correctamente",
      contextDialog: "Se registraron correctamente los datos.",
      picture: Assets.successful,
    ));
  }

  Future<void> _dialogWrongValue() async {
    await showPlatformDialog(
        context: context, builder: (_) => DialogFormApplicant(
      title: "Campos incorrectos",
      contextDialog: "Al parecer alguno de los datos ingresados no es correcto. \nCorrígelo para continuar.",
      picture: Assets.error,));
  }

  Future <bool> _onBackPressed() async{
    if(_name.text.isNotEmpty || _firstLastName.text.isNotEmpty ||
        _firstLastName.text.isNotEmpty && _street.text.isNotEmpty || _numberStreet.text.isNotEmpty
        || _neighborhood.text.isNotEmpty && _cp.text.isNotEmpty || _cellPhone.text.isNotEmpty
        || _rfc.text.isNotEmpty || _cp.text.isNotEmpty || _cellPhone.text.isNotEmpty
        || _rfc.text.isNotEmpty || _listPaths.isNotEmpty || _listNamesDocuments.isNotEmpty
        ||_secondLastName.text.isNotEmpty){
      return (await showPlatformDialog(context: context, builder: (_) => DialogFormApplicant(
        title: "ALERTA",
        isExit: true,
        contextDialog: "Se perderan todos los datos registrados.",
        picture: Assets.warning,))) ?? false;
    }
    return true;
  }

  void _validateButton() {
    setState(() {
      if (_name.text.isNotEmpty && _firstLastName.text.isNotEmpty &&
          _firstLastName.text.isNotEmpty && _street.text.isNotEmpty && _numberStreet.text.isNotEmpty
      && _neighborhood.text.isNotEmpty && _cp.text.isNotEmpty && _cellPhone.text.isNotEmpty
      && _rfc.text.isNotEmpty && _cp.text.length > 4 && _cellPhone.text.length > 9
      && _rfc.text.length > 12 && _listPaths.isNotEmpty && _listNamesDocuments.isNotEmpty) {
        this.buttonEnabled = true;
      } else {
        buttonEnabled = false;
      }
    });
  }
}
