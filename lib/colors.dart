import 'package:flutter/material.dart';

class AppColors{
  static const Color principalColor = const Color(0xffE63946);
  static const Color secondaryColor = const Color(0xff1d3557);
  static const Color backgroundColor = const Color(0xffF1FAEE);
  static const Color backgroundSecondary = const Color(0xffC8D7D2);
  static const Color radioButton = const Color(0xff457B9D);

}